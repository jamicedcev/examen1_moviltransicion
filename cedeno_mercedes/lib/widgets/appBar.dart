import 'package:cedeno_mercedes/backend.dart';
import 'package:cedeno_mercedes/screens/ListScreen.dart';
import 'package:flutter/material.dart';

class appBarFARMACIA extends StatelessWidget {
  const appBarFARMACIA({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String name = 'Cedeño Cevallos Jamileth';
    return Scaffold(
      appBar: AppBar(
        title: Text('$name       8vo'),
        centerTitle: true,
      ),
      body: ListScreen(backend: Backend()),
    );
  }
}
