import 'product.dart';

class Backend {
  /// Singleton pattern used here.
  static final Backend _backend = Backend._internal();

  var products;

  factory Backend() {
    return _backend;
  }

  Backend._internal();

  /// Private list of products. Hardcoded here for testing purposes.
  final _products = [
    Product(
      id: 1,
      nombre: 'QG5 TABx166/1MGx30',
      precio: '20.54',
      dosis:
          'Se recomienda tomar 1 tableta 3 veces al día, durante 1 mes. En la diarrea aguda no infecciosa: 1 tableta, 3 veces al día durante 2 a 3 días. Cólico menstrual: 1 o 2 tabletas cada 8 horas durante el período menstrual.',
      imagen: 'https://seguridad-ca87c.web.app/img/domo2.jpg',
    ),
    Product(
      id: 2,
      nombre: 'LECHE ENFAGROW PREM PROM VAx800G E3',
      precio: '22.08',
      dosis: 'Consulte a su pediatra o en el empaque la cantidad exacta.',
      imagen:
          'https://farmaciascruzazul.ec/images/thumbs/1222010_leche-enfagrow-prem-prom-vax800g-e3.jpeg',
    ),
    Product(
      id: 3,
      nombre: 'IMMUVIT PLUS Q10 CAPx20/80MGx30',
      precio: '270.00',
      dosis:
          'En pacientes hipertensos controlados. Evitar el abuso del ginseng (dosis elevadas por tiempos prolongados). En casos de metrorragia (sangrado uterino).',
      imagen:
          'https://farmaciascruzazul.ec/images/thumbs/1216178_immuvit-plus-q10-capx2080mgx30.jpeg',
    ),
    Product(
      id: 4,
      nombre: 'MENTOL CHINO AEROSOLx150ML',
      precio: '11.27',
      dosis:
          'Aplicar de 3 a 4 veces al día de forma abundante o según la intensidad del dolor.',
      imagen:
          'https://farmaciascruzazul.ec/images/thumbs/1225454_mentol-chino-aerosolx150ml.jpeg',
    ),
    Product(
      id: 5,
      nombre: 'HIRUDOID GELx0.3%x40GR',
      precio: '10.31',
      dosis: 'Evitar el contacto con los ojos y heridas abiertas.',
      imagen:
          'https://farmaciascruzazul.ec/images/thumbs/1213691_hirudoid-gelx03x40gr.jpeg',
    ),
    Product(
      id: 6,
      nombre: 'GYNO-CANESTEN-3 CRE-VAGx2%x20GR',
      precio: '5.70',
      dosis: '1 aplicación diaria durante 3 días.',
      imagen:
          'https://farmaciascruzazul.ec/images/thumbs/1213766_gyno-canesten-3-cre-vagx2x20gr.jpeg',
    ),
    Product(
      id: 7,
      nombre: 'ENJUA BUCAL LISTERINE COOL-Mx180ML',
      precio: '3.33',
      dosis:
          'Indicado para uso diario como complemento de la higiene oral. Enjuagar con 20 ml de Listerine Cool Mint durante 30 segundos en la mañana y en la noche, todos los días.',
      imagen:
          'https://farmaciascruzazul.ec/images/thumbs/1214864_enjua-bucal-listerine-cool-mx180ml.jpeg',
    ),
  ];

  ///
  /// Public API starts here :)
  ///
  /// Returns all products.
  List<Product> getProducts() {
    return _products;
  }

  /// Marks products identified by its id as read.
  void markProductsRead(int id) {
    final index = _products.indexWhere((product) => product.id == id);

    productWidget(product) {}
  }
}
